require 'mina/rails'
require 'mina/git'
require 'mina/rvm'
require 'mina/puma'

set :application_name, 'minanew'
set :domain, 'minanew.com'
set :deploy_to, '/var/www/minanew.com'
set :repository, 'git@gitlab.com:computalya/minanew.git'
set :branch, 'master'

set :user, 'deploy'          # Username in the server to SSH to.
#   set :forward_agent, true     # SSH forward_agent.

# set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/secrets.yml', 'config/puma.rb')
set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/master.key', 'config/puma.rb')

set :rvm_use_path, '/usr/local/rvm/scripts/rvm'
set :shared_dirs, fetch(:shared_dirs, []).push('log', 'tmp/pids', 'tmp/sockets')

task :remote_environment do
  invoke :'rvm:use', 'ruby-2.5.3@default'
end

task :setup do
  in_path(fetch(:shared_path)) do
    command %{mkdir -p tmp/sockets}
    command %{mkdir -p tmp/pids}
  end
end

desc "Deploys the current version to the server."
task :deploy do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      in_path(fetch(:current_path)) do
        command %{mkdir -p tmp/}
        command %{touch tmp/restart.txt}
      end
      invoke :'puma:phased_restart'
    end
  end

  # you can use `run :local` to run tasks on local machine before of after the deploy scripts
  # run(:local){ say 'done' }
end

